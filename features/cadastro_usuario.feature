#language:pt

Funcionalidade: Criar usuário
    Para que eu acessar o portal da finago
    Sendo um usuário
    Posso fazer meu cadastro

    @cadastro
    Esquema do Cenário: Cadastro de usuário
        Um usuário realiza cadastro no portal
        e um novo registro é inserido na gestão de usuários

        Dado que <codigo> é um novo cadastro
        Quando faço o meu cadastro
        Então devo ver a mensagem de cadastro bem sucedido

        Exemplos: 
            | codigo       |
            | "fornecedor"  |
            | "lojistaTEF" |
            | "lojistaPOS" |