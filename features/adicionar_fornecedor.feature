#language:pt

@addFornecedor @loginlojista 
Funcionalidade: Adicionar fornecedor
    Para que eu possa adicionar um fornecedor
    Sendo um usuário que possui uma conta lojista na finago
    Quero adicionar um fornecedor
    
    Esquema do Cenário: Adicionar fornecedor
        Um usuário adiciona um fornecedor
        um novo registro de fornecedor é gerado no grid.

        Dado que <codigo> é um novo fornecedor
        Quando adicionar o fornecedor
        Então devo ver o fornecedor registrado no grid     

        Exemplos: 
            | codigo                        |
            | "fornecedorSaldoVisivel"      |
            | "fornecedorSaldoInvisivel"    |
