class HomePage 
    include Capybara::DSL

    def acessarHome
        find(".v-list-item__title.item", text: 'Home' ).click
    end

    def armazenarSaldo
        contextoSaldo = find('.digital-title.pb-0 .cardContent')

        saldo = 0.0
        contadorSaldoZero = 0;
        while (saldo <= 0.0 && contadorSaldoZero <= 25)
            saldo = contextoSaldo.find('.value').text
            #formatação para "padrão double"
            saldoFormatado = saldo.tr('.', '') 
            saldoFormatado = saldoFormatado.tr(',', '.') 
            saldoFormatado = saldoFormatado.tr(' R$', '')
            saldoDouble = saldoFormatado.to_f
            saldo = saldoDouble
            contadorSaldoZero = contadorSaldoZero+1;
        end   

        return saldo
    end

    def compararSaldo(saldoInicial, saldoFinal, valor)
       testeSaldo = saldoInicial - saldoFinal 
       return true  if(testeSaldo == valor.to_f)
    end

    def usuario_logado
        page.has_css?(".nameUserConfig")
    end
    
    def verificar_perfil(perfil)
        ##verificar regra de verificação de perfil
        page.has_css?(".v-navigation-drawer--open", text: perfil )
        
    end

    def saldoContaDigital
        painelSaldo = find('.col-md-6.col-12', text: 'Saldo Conta Digital')
        buscarSaldo = painelSaldo.find('.value').text
        tratarSaldoArray = buscarSaldo.scan /[^R$\^\s\.]+/
        saldoString = tratarSaldoArray.join()
        saldoConta = saldoString.gsub(",", ".").to_f
    end

    def verificarNome(nome)
        page.has_css?(".v-toolbar__content", text: nome )
    end
      
    def verificarSaldo()
        contextoSaldo = find('.digital-title.pb-0 .cardContent')

        saldo = 0.0
        contadorSaldoZero = 0;
        #gambiarra para dar sleep
        while (saldo <= 0.0 && contadorSaldoZero <=17000)
            saldo = contextoSaldo.find('.value').text
            #formatação para "padrão double"
            saldoFormatado = saldo.tr('.', '') 
            saldoFormatado = saldoFormatado.tr(',', '.') 
            saldoFormatado = saldoFormatado.tr(' R$', '')
            saldoDouble = saldoFormatado.to_f
            saldo = saldoDouble
            contadorSaldoZero = contadorSaldoZero+1
        end 

        return false    if(saldo <= 0.0) 

        return true
    end

end