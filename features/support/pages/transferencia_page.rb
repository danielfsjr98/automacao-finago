class TransferenciaPage 
    include Capybara::DSL
    
    def acessarSessaoTransferencia
        find(".v-list-item__title", text: 'Transferências' ).click
        #click_button "Transferências"
    end

    def verificarSessaoTransferenciaClick
        page.has_css?(".titleTableTransfers")
    end

    def btnFavoritos
        btn = find(".v-btn__content", text: "Favoritos")
    end

    def favoritosVisiveis
        page.has_css?(".v-slide-group__content")
    end

    def procurarCardPorCPF(cnpj_destino)
        find(".dataContactCard", text: cnpj_destino)
    end

    def verificaTelaDadosTransf
        page.has_css?(".title-data", :text => 'Dados da transferência:')
    end

    def inputValorTransf(valor)
        contexto = find('.v-input--is-dirty', text: 'Informe o valor que deseja transferir')
        inputValor = contexto.find('input').set(valor)    
    end

    def btnConfirmar
        find('span.v-btn__content', :text => 'Confirmar')
    end

    def verificarModalConfirmar
        page.has_css?(".v-card__text", wait: 1)
    end

    def btnConfirmarTransf
        find('span.v-btn__content', :text => 'Confirmar Transferência')
    end

    def verificarModalInserirSenha
        page.has_css?(".titlePassword", :text => 'Para completar a transferência, insira a senha da sua conta digital:')
    end

    def inputSenhaDigital(senhaDigital)
        contexto = find('.password-field')
        contexto.find('input').set(senhaDigital)   
    end

    def btnConfirmarTransfFinal
        find('span.v-btn__content', :text => 'Confirmar transferência')
    end
    
    def verificarTransfBemSucedida
        page.has_css?(".titlePassword", :text => 'Transferência realizada')
    end

    def btnFecharTransfBemSucedida
        find('span.v-btn__content', :text => 'Fechar')
    end

    def btnNovaTransf
        click_button 'Nova Transferência'
    end

    def btnOutroBanco
        click_button 'Para outros bancos'
    end

    def verificarUltimaTransfGrid(valor, cnpj)
        #Localização da ultimaTransf no grid
        contextoUltimaTransf = all('.v-data-table__wrapper tbody tr')[0]
        
        #teste de valor apresentado no grid, da ultima transf.
        ultimaTransf = contextoUltimaTransf.text
        valorFormatado = valor.tr('.', ',') 
        testeValor = ultimaTransf.include? valorFormatado 

        #teste do botão "ver" e comparar cnpj da ultima transf
        contextoUltimaTransf.find('.v-btn__content').click
        cnpjFormatado = cnpj.tr('./-', '') 
        testeCnpj = page.has_css?(".v-card", text: cnpjFormatado )

        return true if(testeValor == true && testeCnpj == true) 
        
        return false
    end

    def fecharModalDetalhes
        find('.mdi-window-close').click
    end

    def inputCnpj(cnpj)
        contexto = find('.v-input', text: 'CNPJ')
        contexto.find('input').set(cnpj)   
    end

    def inputApelido(apelido)
        contexto = find('.v-input', text: 'Apelido')
        contexto.find('input').set(apelido)
    end

    def btnSalvarFavorito
        find('.mdi-star-outline').click
    end

    def inputBanco(banco)
        inputBank = find('.v-select__slot', text: 'Banco')
        inputBank.click
        contextoDrop = find('.v-select-list')
        contextoDrop.find('.v-list-item__title', text: banco).click    
    end

    def inputAgencia(agencia)
        contexto = find('.v-input', text: 'Agência')
        contexto.find('input').set(agencia)   
    end

    def inputConta(conta)
        contexto = find('.v-input', text: 'Conta')
        contexto.find('input').set(conta)   
    end

    def inputTipoConta(tipoConta)
        inputTipoC = find('.v-select__selections')
        inputTipoC.click
        contextoDrop = find('.v-select-list')
        contextoDrop.find('.v-list-item__title', text: tipoConta).click    
    end

    def inputNomeTitular(nomeTitular)
        contexto = find('.v-input', text: 'Titular')
        contexto.find('input').set(nomeTitular)  
    end

    def transferirParaFavorito(dadosTransf)
        acessarSessaoTransferencia
        resultSessaoTransf = verificarSessaoTransferenciaClick
        btnFavoritos.click
        resultFavoritos = favoritosVisiveis
        procurarCardPorCPF(dadosTransf['cnpj']).click
        resultDadosTransf = verificaTelaDadosTransf
        inputValorTransf(dadosTransf['valorTransf'])
        btnConfirmar.click
        while(verificarModalConfirmar == false)
            btnConfirmar.click
        end
        btnConfirmarTransf.click
        inputSenhaDigital(CONFIG["users"]["lojista"]["passContaDigital"])
        sleep 1
        btnConfirmarTransfFinal.click
        resultTransf = verificarTransfBemSucedida
        btnFecharTransfBemSucedida.click
    end

    def transferirParaOutroBanco(dadosTransf)
        acessarSessaoTransferencia
        resultSessaoTransf = verificarSessaoTransferenciaClick
        btnNovaTransf
        btnOutroBanco
        inputCnpj(dadosTransf['cnpj'])
        inputNomeTitular(dadosTransf['nomeTitular'])
        inputApelido(dadosTransf['apelido'])
        inputBanco(dadosTransf['banco'])
        inputAgencia(dadosTransf['agencia'])
        inputConta(dadosTransf['conta'])
        inputTipoConta(dadosTransf['tipoConta'])
        inputValorTransf(dadosTransf['valorTransf'])
        btnSalvarFavorito   if(dadosTransf['adicionarFav'])
        btnConfirmar.click
        btnConfirmarTransf.click
        inputSenhaDigital(CONFIG["users"]["lojista"]["passContaDigital"])
        sleep 1
        btnConfirmarTransfFinal.click
        btnFecharTransfBemSucedida.click
    end

    def abastecerConta(cnpjOrigem, valor)
        acessarSessaoTransferencia
        resultSessaoTransf = verificarSessaoTransferenciaClick
        btnFavoritos.click
        resultFavoritos = favoritosVisiveis
        procurarCardPorCPF(cnpjOrigem).click
        resultDadosTransf = verificaTelaDadosTransf
        inputValorTransf(valor)
        btnConfirmar.click
        while(verificarModalConfirmar == false)
            btnConfirmar.click
        end
        btnConfirmarTransf.click
        inputSenhaDigital(CONFIG["users"]["lojista"]["passContaDigital"])
        sleep 1
        btnConfirmarTransfFinal.click
        resultTransf = verificarTransfBemSucedida
        btnFecharTransfBemSucedida.click
    end
    
  end
  