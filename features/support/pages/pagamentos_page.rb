class PagamentosPage
    
    include Capybara::DSL
    
    def fazerPagamento(pagamento)
        puts pagamento
        acessarSessaoPagamentos
        inputCodigoBoleto(pagamento['codigo'])
        btnContinuar
        btnConfirmar
        inputSenhaDigital(CONFIG["users"]["lojista"]["passContaDigital"])
        sleep 1
        clickConfirmarPagamento
    end

    def acessarSessaoPagamentos
        #está achando dois Pagamentos no menu, ver se tem forma melhor para implementar
        all('.v-list-item__title', text: 'Pagamentos')[0].click
    end
    
    def btnContinuar
        find('.button-continue').click
    end

    def inputCodigoBoleto(codigoBoleto)
        contexto = find('.v-text-field__slot', text: 'Insira o código de barras')
        contexto.find('textarea').set(codigoBoleto)
    end

    def btnConfirmar
        find('.v-btn__content', text: 'Confirmar').click
    end

    def inputSenhaDigital(senhaDigital)
        contexto = find('.contentCard')
        contexto.find('input').set(senhaDigital)
    end

    def clickConfirmarPagamento
        click_button 'Confirmar Pagamento'
    end

    def verificarModalPagamentoEfetuado
        page.has_css?('.v-card__text', text: 'Pagamento Efetuado',  wait: 10)
    end

    def verificarUltimoPagamento(codigoBarras)
        contextoLinha = all('tr')[1]
        contextoLinha.find('.v-btn__content', text: 'VER').click
        page.has_css?('v-card__text', codigoBarras)
    end
    
  end
  