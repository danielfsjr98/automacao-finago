class CadastroPage
    include Capybara::DSL

    ##O PRINCIPAL PARA O CADASTRO SERÁ FAZER UM TESTE DE "MASSA SEMPRE VÁLIDA"
    ##OU SEJA, EXCLUIR O CADASTRO ANTES DE REALIZA-LO.

    def fazerCadastro(cadastro)
        visit 'https://stage-app.finago.com.br/registration#'

        #ETAPA: Dados cadastrais
        inputCnpj(cadastro['cnpj'])   
        testeBtnVisivel = find('.v-btn__content', text: 'Alterar CNPJ')
        btn_continuar

        #ETAPA: Perfil da Empresa
        inputPerfilDaEmpresa(cadastro['perfilEmpresa'])

        if(cadastro['perfilEmpresa'].eql? "Lojista") 
            inputSolCaptura(cadastro['solCaptura'])
            if(cadastro["solCaptura"].eql? 'Máquina de Cartão (POS)')
                inputQtdPOS(cadastro['qtdPOS'])
            end
            inputFornecedores(cadastro['fornecedores'])
        end

        inputFaturamentoMensal(cadastro['faturamentoMensal'])
        inputSegmento(cadastro['segmento'])
        inputTelefone(cadastro['telefone'])
        inputCelular(cadastro['celular'])
        btn_continuar

        #ETAPA: Dados do representante
        inputNomeRepresentante(cadastro['nomeRepresentante'])
        inputNomeMaeRepresentante(cadastro['nomeMaeRepresentante'])
        inputCpfRepresentante(cadastro['cpfRepresentante'])
        inputDataNascRepresentante(cadastro['dataNascRepresentante'])
        btn_continuar

        #ETAPA: Endereço do Representante
        contextoEnd = find('.v-form', text: 'CEP')
        inputCEP(cadastro['cepRepresentante'])
        inputNumCasa(cadastro['numeroCasaRepresentante'])
        inputComplemento(cadastro['complementoCasaRepresentante'])
        btn_continuar

        # Caso demore pra encontrar o cpf, entra no If abaixo.
        auxTeste = page.has_text?('obrigatório', wait: 1)
        if (auxTeste) 
            contador = 0     
            while (contextoEnd.has_text?('obrigatório'))
                btn_continuar
                if(contador>20)   
                    puts "Problema no preenchimento do endereço"
                    break  
                end                                                
                contador = contador + 1                            
            end   
        end                                                        

        #ETAPA: Informações do pagamento
        inputBanco(cadastro['banco'])
        inputAgencia(cadastro['agencia'])
        inputConta(cadastro['conta'])
        inputTipoConta(cadastro['tipoConta'])
        btn_continuar

        #ETAPA: Acesso Finago
        inputEmail(cadastro['email'])
        inputConfirmEmail(cadastro['email'])
        inputSenha(cadastro['senha'])
        inputConfirmSenha(cadastro['senha'])
        inputAceite()
        sleep 2
    end

    def inputCnpj(cnpj)
        contexto = find('.v-input__slot', text: 'CNPJ')
        contexto.find('input').set(cnpj)
    end

    def btn_continuar
        click_button 'Continuar'
    end

    def inputPerfilDaEmpresa(perfil)
        find('.v-select__slot', text: 'Perfil da Empresa').click
        contextoDrop = find('.v-select-list')
        contextoDrop.find('.v-list-item__title', text: perfil).click
    end

    def inputFaturamentoMensal(faturamento)
        contexto = find('.v-input__slot', text: 'Faturamento Mensal')
        contexto.find('input').set(faturamento)
    end

    def inputSegmento(segmento)
        find('.v-select__slot', text: 'Segmento').click
        contextoDrop = find('.v-select-list')
        contextoDrop.find('.v-list-item__title', text: segmento).click
    end

    def inputSolCaptura (solCaptura)
        find('.v-select__slot', text: 'Solução de Captura').click
        contextoDrop = find('.v-select-list')
        contextoDrop.find('.v-list-item__title', text: solCaptura).click
    end

    def inputQtdPOS(qtdPOS)
        find('.v-select__slot', text: 'Quantidade de POS').click
        contextoDrop = find('.v-select-list')
        contextoDrop.find('.v-list-item__title', text: qtdPOS).click   
    end

    def inputFornecedores(fornecedores)
        
        #aqui tá dando elemento não encontrado
        #problema encontrado, os elementos aparecem conforme rola para baixo o dropdown
        #portanto, não encontra antes de rolar.
        puts fornecedores

        contextoGeral = find('.v-select__slot', text: 'Selecione os seus fornecedores').click
        contextoDrop = find('.menuable__content__active')

        fornecedores.each do |f|  
            while(page.has_css?('.v-list-item__title', text: f) == false)
                puts 'entrou aqui po :D'
               # page.native.send_keys(:arrow_down)
            end

            find('.v-list-item__title', text: f).click 
        end

        contextoGeral.find('.mdi-menu-down').click  
        
    end


    def inputTelefone(telefone)
        contexto = find('.v-input__slot', text: 'Telefone')
        contexto.find('input').set(telefone)
    end

    def inputCelular(celular)
        contexto = find('.v-input__slot', text: 'Celular')
        contexto.find('input').set(celular)
    end

    def inputNomeRepresentante(nomeRepresentante)
        contexto = find('.v-input__slot', text: 'Nome do representante')
        contexto.find('input').set(nomeRepresentante)
    end

    def inputNomeMaeRepresentante(nomeMaeRepresentante)
        contexto = find('.v-input__slot', text: 'Nome da mãe do Representante')
        contexto.find('input').set(nomeMaeRepresentante)
    end

    def inputCpfRepresentante(cpfRepresentante)
        contexto = find('.v-input__slot', text: 'CPF do Representante')
        contexto.find('input').set(cpfRepresentante)
    end

    def inputDataNascRepresentante(dataNascRepresentante)
        contexto = find('.v-input__slot', text: 'Data de Nascimento')
        contexto.find('input').set(dataNascRepresentante)
    end

    def inputCEP(cepRepresentante)
        contexto = find('.v-input__slot', text: 'CEP')
        contexto.find('input').set(cepRepresentante)
    end

    def inputNumCasa(numeroCasaRepresentante)
        contexto = find('.v-input__slot', text: 'Número')
        contexto.find('input').set(numeroCasaRepresentante)
    end
    
    def inputComplemento(complementoCasaRepresentante)
        contexto = find('.v-input__slot', text: 'Complemento')
        contexto.find('input').set(complementoCasaRepresentante)
    end

    def inputBanco(banco)
        find('.v-select__slot', text: 'Banco').click
        contextoDrop = find('.v-select-list')
        contextoDrop.find('.v-list-item__title', text: banco).click
    end
    
    def inputAgencia(agencia)
        contexto = find('.v-input__slot', text: 'Agência')
        contexto.find('input').set(agencia)
    end

    def inputConta(conta)
        find("input[placeholder='Inserir Conta']").set(conta)
    end

    def inputTipoConta(tipoConta)
        find('.v-select__slot', text: 'Tipo de Conta').click
        contextoDrop = find('.v-select-list')
        contextoDrop.find('.v-list-item__title', text: tipoConta).click
    end

    def inputEmail(email)
        find("input[placeholder='Insira seu e-mail']").set(email)
    end

    def inputConfirmEmail(email)
        find("input[placeholder='Insira novamente seu e-mail']").set(email)
    end

    def inputSenha(senha)
        find("input[placeholder='Inserir Senha']").set(senha)
    end

    def inputConfirmSenha(senha)
        find("input[placeholder='Confirmar senha']").set(senha)
    end

    def inputAceite
        find('.v-input--selection-controls__ripple').click
    end

end