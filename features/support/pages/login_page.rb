class LoginPage 

  include Capybara::DSL

  def logar(cnpj, senha)
    visit "/login"
    fill_in :placeholder => 'CNPJ', :with => cnpj
    fill_in :placeholder => 'Senha', :with => senha
    click_button "Acessar"
    fecharPix
  end

  def alert
    find(".v-alert__content").text
  end

  def fecharPix
    if(page.has_css?('.contentCard', text: 'Pix'))
      contexto = find('.contentCard', text: 'Pix')
      contexto.find('.mdi-window-close').click
    end
  end

end
