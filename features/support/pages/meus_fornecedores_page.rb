class MeusFornecedoresPage
  include Capybara::DSL

  def adicionarFornecedor(fornecedor)
    acessarPageMeusFornecedores
    btnAddFornecedor
    inputCnpj(fornecedor['cnpj'])
    mostrarSaldo(fornecedor['visivel'])
    btnSalvar
    btnFechar
  end

  def acessarPageMeusFornecedores
    contexto = find('.v-navigation-drawer__content', text:'Meus Fornecedores')
    contexto.find('.v-list-item__title', text:'Meus Fornecedores').click
  end
  
  def btnAddFornecedor
    find(".btnNewRequest", text:'Adicionar Fornecedor').click
  end

  def inputCnpj(cnpj)
    contexto = find('.v-input', text: 'CNPJ')
    contexto.find('input').set(cnpj)
  end

  def mostrarSaldo(mostrar)
    find('.v-select__selections').click
    contextoDrop = find('.v-menu__content')
    if mostrar == true
      contextoDrop.find('.v-list-item__content', text: "Sim").click
    else 
      contextoDrop.find('.v-list-item__content', text: "Não").click
    end
  end

  def btnSalvar
    find('.v-btn__content', text: "SALVAR").click
  end

  def btnFechar
    find('.btnClose', text: "Fechar").click
  end

  def verificarCadastro(fornecedor)
    contextoGrid = find('.v-data-table__wrapper', text: 'CNPJ')
    contextoGrid.has_css?('tr', text: fornecedor['cnpj'])
  end

  ## METODOS PARA DEIXAR MASSA SEMPRE VALIDA

  def excluirFornecedor(fornecedor)
    acessarPageMeusFornecedores
    resultado = procurarNoGrid(fornecedor)
    if(resultado.blank? == false)
      resultado.find('.delete-icon').click
      confirmarExclusao 
      # O problema está em: As excluir funciona, as vezes não. https://monosnap.com/direct/Tl9i1c8I9wOunud5AN6LMA7zgJUtLp
      fecharModalExcluir
    end
  end

  def procurarNoGrid(fornecedor)
    contextoGrid = find('.v-data-table__wrapper', text: 'CNPJ')
    contemElemento = contextoGrid.has_css?('tr', text: fornecedor['cnpj'], wait: 1)

    if(contemElemento)
      return fornecedorAlvo = contextoGrid.find('tr', text: fornecedor['cnpj'])
    end
    return nil
  end

   ## O PROBLEMA ESTÁ ACONTECENDO, POIS O SITE ESTÁ BUGADO: AS VEZES PEDE PRA CONFIRMAR EXCLUSÃO, AS VEZES NÃO
  def confirmarExclusao
    contexto = find('.v-card__actions', text: "SIM")
    contexto.find('.v-btn__content', text: "SIM").click
  end

  def fecharModalExcluir
    sleep 5
    puts 'chegou aqui'
    #contexto = find('.contentCard', text: "Deletar Fornecedor")
    find('.mdi-close').click
  end

end
  