require 'report_builder'
require 'date'

Before do
    @login_page = LoginPage.new
    @transferencia_page = TransferenciaPage.new
    @home_page = HomePage.new
    @cadastro_page = CadastroPage.new
    @pagamento_page = PagamentosPage.new
    @meus_fornecedores_page = MeusFornecedoresPage.new
    page.current_window.resize_to(1440, 900)
end

Before("@loginlojista") do
     user = CONFIG["users"]["lojista"]
     @login_page.logar(user["cnpj"], user["pass"])
end

Before("@loginfornecedor") do
    user = CONFIG["users"]["lojista"]
    @login_page.logar(user["cnpj"], user["pass"])
end

Before("@loginadmin") do
    user = CONFIG["users"]["lojista"]
    @login_page.logar(user["email"], user["pass"])
end

After do |scenario|
    screenshot = page.save_screenshot("logs/screenshots/#{scenario.__id__}.png")
    attach(screenshot, "image/png")
end

## Métodos abaixo para gerar report via reportbuilder

# After do
#     temp_shot = page.save_screenshot('logs/screenshots/tempshot.png')
#     screenshot = Base64.encode64(File.open(temp_shot, "rb").read)
#     attach(screenshot, "image/png")
# end

# current_date = DateTime.now
# @data_formatada = current_date.strftime("%d/%m/%Y") #formatação horario
# @hora_formatada = current_date.strftime("%I:%M%p") #formatação de hora
# @current_date_formatado = "#{@data_formatada.to_s.tr('/', '-')}_#{@hora_formatada.to_s.tr(':', '-')}"

# at_exit do
#     ReportBuilder.configure do |config|
#     config.input_path = 'logs/report.json'
#     config.report_path = 'logs/report_'+@current_date_formatado
#     config.report_types = [:html]
#     config.report_title = 'Resultado Testes - Finago'
#     config.compress_image = true
#     #a linha abaixo é para configurar qualquer info que vc ache necessario
#     config.additional_info = {browser: ENV['browser'], environment: 'Teste de validação Daniel', "Data de execução" => @data_formatada, "Hora de execução" => @hora_formatada} 
#     config.color = 'indigo'
#     end
#     ReportBuilder.build_report
# end