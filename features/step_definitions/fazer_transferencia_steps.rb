Dado('que {string} é uma nova transferencia') do |codetransf|
  endereco = "features/support/fixtures/transferencia.yaml"
  endereco = endereco.tr("/", "\\")   if OS.windows?
  file = YAML.load_file(File.join(Dir.pwd, endereco)) 
  @transf = file[codetransf]
  @valor = @transf['valorTransf']
end

Dado('eu possua uma conta com saldo maior que vinte') do
  # Lógica: Pegar saldo da conta destino e passar pra conta origem.
  @login_page.logar(@transf["cnpj"], "Nike100!")
  @transferencia_page.abastecerConta(CONFIG["users"]["lojista"]["cnpj"], "20.00")
end
  
Quando('realizo a transferencia a um favorito') do
  visit 'https://stage-app.finago.com.br/login'
  @login_page.logar(CONFIG["users"]["lojista"]["cnpj"], CONFIG["users"]["lojista"]["pass"])
  @saldoInicial = @home_page.armazenarSaldo
  @transferencia_page.transferirParaFavorito(@transf)
end

Quando('realizo a transferencia a um outro banco') do
  @saldoInicial = @home_page.armazenarSaldo
  @transferencia_page.transferirParaOutroBanco(@transf)
end

Então('devo ver a transferencia registrada no grid') do
  sleep 3 # sleep para aguardar a transferência registrar no grid.
  testeTransf = @transferencia_page.verificarUltimaTransfGrid(@transf['valorTransf'], @transf['cnpj'])
  expect(testeTransf).to be true
  @transferencia_page.fecharModalDetalhes
end

Então('devo comparar saldo final com inicial') do
  @home_page.acessarHome
  @saldoFinal = @home_page.armazenarSaldo
  log(@saldoInicial)
  log(@saldoFinal)
  testeSaldo = @home_page.compararSaldo(@saldoInicial, @saldoFinal, @valor)
  expect(testeSaldo).to be true
end
