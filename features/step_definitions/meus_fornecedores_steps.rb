Dado('que {string} é um novo fornecedor') do |codefornecedor|
    endereco = "features/support/fixtures/novoFornecedor.yaml"
    endereco = endereco.tr("/", "\\")   if OS.windows?
    file = YAML.load_file(File.join(Dir.pwd, endereco)) 
    @fornecedor = file[codefornecedor]
end
  
Quando('adicionar o fornecedor') do
    @meus_fornecedores_page.excluirFornecedor(@fornecedor)
    @meus_fornecedores_page.adicionarFornecedor(@fornecedor)
end
  
Então('devo ver o fornecedor registrado no grid') do
    teste = @meus_fornecedores_page.verificarCadastro(@fornecedor)
    expect(teste).to be true
end