
Dado('que {string} é um novo cadastro') do |cadastro_code|
  endereco = "features/support/fixtures/cadastro.yaml"
  endereco = endereco.tr("/", "\\")   if OS.windows?
  file = YAML.load_file(File.join(Dir.pwd, endereco))
  @cadastro = file[cadastro_code]
end

Quando('faço o meu cadastro') do
  @cadastro_page.fazerCadastro(@cadastro)
end

Então('devo ver a mensagem de cadastro bem sucedido') do
  pending # Write code here that turns the phrase above into concrete actions
end