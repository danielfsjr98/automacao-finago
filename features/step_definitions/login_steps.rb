Dado("que possuo o cnpj {string}") do |cnpj|
  @cnpj = cnpj
end

Dado("possuo a senha {string}") do |senha|
  @senha = senha
end

Quando("eu entro no portal Finago") do
  login = LoginPage.new
  login.logar(@cnpj, @senha)
end

Entao("devo ver a area logada") do
  expect(@home_page.usuario_logado).to be true
end

Entao("devo ver a mensagem de alerta: {string}") do |mensagem_esperada|
  login = LoginPage.new
  expect(login.alert).to eql mensagem_esperada
end

Entao("devo ver a mensagem campo obrigatorio: {string}") do |mensagem_esperada|
  teste  = page.has_content?(mensagem_esperada) 
  expect(teste).to be true
end

Entao("verifico se minha conta e de {string}") do |perfil|
  #hint, verificar perfil é readonly
  testePerfil = @home_page.verificar_perfil(perfil)
  expect(testePerfil).to be true
end

Entao('verifico meus dados na home {string}') do |nome|
  testeNome = @home_page.verificarNome(nome)
  expect(testeNome).to be true
  testeSaldo = @home_page.verificarSaldo()
  expect(testeSaldo).to be true
end