Dado('que eu possua um valor {string} de depósito') do |valor|
    find(".v-list-item__title.item", text: 'Depósitos' ).click
    @valor = valor
end
  
Quando('eu informar o valor') do
    contexto = find(".v-text-field__slot", text: 'Informe')
    contexto.find("#form").set(@valor)
    find('.v-btn__content', text: 'Gerar Boleto').click
    sleep 5
end
  
Então('devo ver o boleto gerado na tela') do
    @boleto = find('.v-card__text.modal-form')
    expect(@boleto).to have_text('Novo boleto gerado com sucesso!')
end
  
Então('o valor deve conferir') do
    contextoValor = @boleto.find('.pt-0.pb-0.col-md-8', text: 'Valor')
    valorBoleto = contextoValor.find('.text-value').text
    #formatação para "padrão double"
    valorBoletoFormatado = valorBoleto.tr('.', '') 
    valorBoletoFormatado = valorBoletoFormatado.tr(',', '.') 
    valorBoletoFormatado = valorBoletoFormatado.tr(' R$', '')
    valorBoletoFormatado = valorBoletoFormatado.to_f
    expect(@valor.to_f).to eql(valorBoletoFormatado)
end