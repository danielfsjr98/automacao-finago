Dado('que "{string}" é um novo pagamento') do |codepagamento|
    endereco = "features/support/fixtures/pagamento.yaml"
    endereco = endereco.tr("/", "\\")   if OS.windows?
    file = YAML.load_file(File.join(Dir.pwd, endereco)) 
    @pagamento = file[codepagamento]
end
  
Quando('realizo o pagamento') do
    @saldoInicial = @home_page.armazenarSaldo
    @valor = @pagamento['valor']
    @pagamento_page.fazerPagamento(@pagamento)
end
  
Então('devo ver o pagamento registrado no historico de pagamentos') do
    booleanPagamento = @pagamento_page.verificarModalPagamentoEfetuado
    expect(booleanPagamento).to be true
    testeGrid = @pagamento_page.verificarUltimoPagamento(@pagamento['codigo'])
    expect(testeGrid).to be true
end
  