#language:pt

@pagamento @loginlojista
Funcionalidade: Fazer pagamento
    Para que eu possa pagar um boleto
    Sendo um usuário que possui uma conta na finago
    Quero poder pagar um boleto usando meu saldo

    Esquema do Cenário: Fazer pagamento de boleto

        Dado que "<codigo>" é um novo pagamento
        Quando realizo o pagamento
        Então devo ver o pagamento registrado no historico de pagamentos
        E devo comparar saldo final com inicial

         Exemplos: 
            | codigo                |
            | "bolConsumoFormatado" |
            #| "bolConsumo"          |
