#language:pt

@jenkins @transf 
Funcionalidade: Fazer transferência
    Para que eu possa fazer transferência
    Sendo um usuário que possui uma conta na finago
    Quero transferir meu saldo parcial ou total a outra pessoa.

    @favorito
    Esquema do Cenário: Transferência para favorito
        Um usuário faz uma transferência a um contato favorito
        um novo registro de transferência é gerado.

        Dado que <codigo> é uma nova transferencia
        E eu possua uma conta com saldo maior que vinte
        Quando realizo a transferencia a um favorito
        Então devo ver a transferencia registrada no grid
        E devo comparar saldo final com inicial
        

        Exemplos: 
            | codigo        |
            | "favorito"    |

    @outroBanco @loginlojista
    Esquema do Cenário: Transferência para outro banco
        Um usuário faz uma transferência a um outro banco
        um novo registro de transferência é gerado.

        Dado que <codigo> é uma nova transferencia
        Quando realizo a transferencia a um outro banco
        Então devo ver a transferencia registrada no grid
        E devo comparar saldo final com inicial
        

        Exemplos: 
            | codigo     |
            | "outroBanco" |
