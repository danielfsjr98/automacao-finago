#language:pt

@jenkins @gerarBoleto @loginlojista 
Funcionalidade: Adicionar fornecedor
    Para que eu possa realizar um depósito na minha conta digital
    Sendo um usuário que possui uma conta na finago
    Quero gerar um boleto com o valor solicitado.
    
    Cenário:
        Dado que eu possua um valor '20.00' de depósito
        Quando eu informar o valor
        Então devo ver o boleto gerado na tela  
        E o valor deve conferir
