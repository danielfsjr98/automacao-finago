#language:pt

@jenkins @acesso 
Funcionalidade: Acesso
    Para que eu possa ver as funcionalidades do portal finago
    Sendo um usuário que possui uma conta na finago
    Quero poder iniciar uma nova sessão
    
    @login_happy
    Esquema do Cenario: Nova sessao com lojista e fornecedor 

        Dado que possuo o cnpj "<cnpj>"
        E possuo a senha "<senha>"
        Quando eu entro no portal Finago
        Entao devo ver a area logada
        E verifico se minha conta e de "<menu_esperado>"
        E verifico meus dados na home "<nome>"

        Exemplos:
            | cnpj               |  senha     | menu_esperado     | nome    |
            | 11.774.839/0001-01 |  Nike100!  | Agenda Lojista    | DANIEL  |
            | 22.183.895/0001-80 |  Nike100!  | Meus Fornecedores | DANIEL  |

    Esquema do Cenario: Tentar logar com usuario errado

        Dado que possuo o cnpj "<cnpj>"
        E possuo a senha "Nike100!"
        Quando eu entro no portal Finago
        Entao devo ver a mensagem de alerta: "Falha na validação, CNPJ ou senha incorreto"

        Exemplos:
            | cnpj               |
            | 23.234.775/0001-01 |
            | 23.234.775/0001-02 |

    Esquema do Cenario: Tentar logar sem incluir cnpj e senha

        Dado que possuo o cnpj "<cnpj>"
        E possuo a senha "<senha>"
        Quando eu entro no portal Finago
        Entao devo ver a mensagem campo obrigatorio: "<mensagem_esperada>"

        Exemplos:
            | cnpj                | senha    | mensagem_esperada |   
            |                     | Nike100! | Login obrigatório |
            | 23.234.775/0001-27  |          | Senha obrigatória | 