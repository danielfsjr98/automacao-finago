# finago
Repositório do projeto Finago com Cucumber, Capybara e Ruby

## Como executar o projeto

* Importante ter o Ruby instalado (v 2.5 ou superior)

### Instalar o bundler
'
gem install bundler
'

### Instalar as dependências do Ruby
'
bundle install
'

### Executar localmente (minha máquina)
'
bundle exec cucumber
'

### Executar no servidor de CI (Gerando reports json)
'
bundle exec cucumber -p ci
'
